// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

InternetButton button = InternetButton();

void setup() {
  button.begin();

 // sow precipitation
  Particle.function("changeTheColor", changeTheColor);
  // show the temperature
  Particle.function("showTemperature", showTemperature);
  
  // show the time

  Particle.function("showTime", showTime);

}
void loop() {

}
int changeTheColor(String cmd) {
  if (cmd == "Grey") {
    button.allLedsOn(169,169,169);
    delay(2000);
    button.allLedsOff();
  }
  else if (cmd == "Yellow") {
    button.allLedsOn(255,255,0);
    delay(2000);
    button.allLedsOff();
  }
  else if (cmd == "White") {
    button.allLedsOn(255,255,255);
    delay(2000);
    button.allLedsOff();
  }
  else if (cmd == "Blue") {
    button.allLedsOn(17,33,253);
    delay(2000);
    button.allLedsOff();
  }
  // haze
  else if (cmd == "Red") {
    button.allLedsOn(255,0,0);
    delay(2000);
    button.allLedsOff();
  }
  //fog
  else if (cmd == "Pink") {
    button.allLedsOn(255,6,255);
    delay(2000);
    button.allLedsOff();
  }
  else {
    // you received an invalid return -1
    return -1;
  }
  // function succesfully finished
  return 1;
}


int showTime(String cmd) {
   
   int inTime = cmd.toInt();
   
   int inHours = inTime/100;
    
    
    int inMinutes = inTime%100;
    
    // hours.........
    
    int firstLetterH = inHours/10;
    int secondLetterH = inHours%10;
    
    if(firstLetterH <= 0){
        for (int i = 1; i <= secondLetterH; i++) {
      button.ledOn(i, 17, 255, 0);
  }
  delay(2000);
    button.allLedsOff();
    }
    
    
    else if(firstLetterH > 0 && firstLetterH <= 2 ){
        for (int i = 1; i <= firstLetterH; i++) {
      button.ledOn(i, 255, 0, 0);
  }
  delay(2000);
    button.allLedsOff();
    
    for (int i = 1; i <= secondLetterH; i++) {
      button.ledOn(i, 17, 255, 0);
  }
  delay(2000);
    button.allLedsOff();
    delay(2000);
    }
        
    else {
        return -1;
    }
    


 // return 1;
    
    // minutes......
    
    int firstLetter = inMinutes/10;
    int secondLetter = inMinutes%10;
    
    if(firstLetter <= 0){
        for (int i = 1; i <= secondLetter; i++) {
      button.ledOn(i, 255, 255, 0);
  }
  delay(2000);
    button.allLedsOff();
    }
    
    
    else if(firstLetter > 0 && firstLetter <= 5 ){
        for (int i = 1; i <= firstLetter; i++) {
      button.ledOn(i, 17, 33, 253);
  }
  delay(2000);
    button.allLedsOff();
    
    for (int i = 1; i <= secondLetter; i++) {
      button.ledOn(i, 255, 255, 0);
  }
  delay(2000);
    button.allLedsOff();
    }
        
    else {
        return -1;
    }
    


  return 1;
}

int showTemperature(String cmd) {
      delay(2000);
    int inTemperature = cmd.toInt();
    
    // for negative temperature
    if(inTemperature <= 0){
        button.allLedsOn(255,255,255);
            delay(2000);
            button.allLedsOff();
            delay(2000);
            
            inTemperature = -1*inTemperature;
        
    }
    
    int firstLetter = inTemperature/10;
    int secondLetter = inTemperature%10;
    
    if(firstLetter <=0){
        for (int i = 1; i <= secondLetter; i++) {
      button.ledOn(i, 255, 255, 0);
  }
  delay(2000);
    button.allLedsOff();
    }
    
    
    else if(firstLetter > 0 && firstLetter <= 9 ){
        for (int i = 1; i <= firstLetter; i++) {
      button.ledOn(i, 17, 33, 253);
  }
  delay(2000);
    button.allLedsOff();
    
    for (int i = 1; i <= secondLetter; i++) {
      button.ledOn(i, 255, 255, 0);
  }
  delay(2000);
    button.allLedsOff();
    }
        
    else {
        return -1;
    }
    


  return 1;
}
















