//
//  InterfaceController.swift
//  Activity3wearable WatchKit Extension
//
//  Created by Prabhjinder Singh on 2019-11-01.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    
    @IBOutlet weak var counteryNamesTable: WKInterfaceTable!
    
    let countriesList:[String] = ["America/Toronto", "America/Vancouver", "Europe/London","Asia/Kolkata"]

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        self.counteryNamesTable.setNumberOfRows(
            self.countriesList.count, withRowType:"myRow"
        )
      
        for (index, country) in self.countriesList.enumerated() {
            let row = self.counteryNamesTable.rowController(at: index) as! CountryRowController
            row.countryNameLabel.setText(country)
        }
        //connect to phone
        super.willActivate()
        if(WCSession.isSupported() == true){
            print("watch support WC session")
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        }
        else{
            print("watch do not support wc session")
            
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        print(self.countriesList[rowIndex])
        // send message to phone
        if(WCSession.default.isReachable == true){
           let message = ["countryName":rowIndex] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("message sent to phone from watch")
        }
        else{
            print("message not sent from the watch")
        }
    }
    
    
    
 
    
}
